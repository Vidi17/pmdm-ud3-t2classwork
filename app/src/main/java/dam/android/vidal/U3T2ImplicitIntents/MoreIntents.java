package dam.android.vidal.U3T2ImplicitIntents;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.actions.NoteIntents;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener {

    private EditText alarmMessage, alarmHour, alarmMinutes, noteTitle, noteText, phoneNumber;
    public static final String IMPLICIT_INTENTS = "ImplicitIntents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_intents);

        setUI();
    }

    private void setUI(){
        Button btAlarm, btNote, btPhoneCall;

        alarmMessage = findViewById(R.id.alarmMessage);
        alarmHour = findViewById(R.id.alarmHour);
        alarmMinutes = findViewById(R.id.alarmMinutes);

        noteTitle = findViewById(R.id.etNoteTitle);
        noteText = findViewById(R.id.etNoteText);

        phoneNumber = findViewById(R.id.etPhoneCall);

        btAlarm = findViewById(R.id.btAlarm);
        btNote = findViewById(R.id.btNote);
        btPhoneCall = findViewById(R.id.btPhoneCall);


        btAlarm.setOnClickListener(this);
        btNote.setOnClickListener(this);
        btPhoneCall.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btAlarm:
                createAlarm(alarmMessage.toString(), Integer.parseInt(alarmHour.toString())
                        , Integer.parseInt(alarmMinutes.toString()));
                break;
            case R.id.btNote:
                createNote(noteTitle.toString(), noteText.toString());
                break;
            case R.id.btPhoneCall:
                dialPhoneNumber(phoneNumber.toString());
                break;
        }
    }

    public void createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "CreateAlarm : Can't handle this intent!");
        }
    }

    public void createNote(String subject, String text) {
        Intent intent = new Intent(NoteIntents.ACTION_CREATE_NOTE)
                .putExtra(NoteIntents.EXTRA_NAME, subject)
                .putExtra(NoteIntents.EXTRA_TEXT, text);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else {
            Log.d(IMPLICIT_INTENTS, "CreateNote : Can't handle this intent!");
        }
    }

    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else {
            Log.d(IMPLICIT_INTENTS, "DialPhoneNumber : Can't handle this intent!");
        }
    }
}