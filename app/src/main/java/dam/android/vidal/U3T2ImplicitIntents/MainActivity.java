package dam.android.vidal.U3T2ImplicitIntents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private EditText etUri, etLocation, etText, etZoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){
        Button btOpenUri, btShareText, btOpenLocation;

        etUri = findViewById(R.id.etUri);
        etLocation = findViewById(R.id.etLocation);
        etText = findViewById(R.id.etText);
        etZoom = findViewById(R.id.etZoom);

        btOpenLocation = findViewById(R.id.btOpenLocation);
        btOpenUri = findViewById(R.id.btOpenUri);
        btShareText = findViewById(R.id.btShareText);

        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShareText.setOnClickListener(this);
        Button btNextActivity = findViewById(R.id.btNextActivity);

        btNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MoreIntents.class));
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btOpenUri:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.btShareText:
                shareText(etText.getText().toString());
                break;
            case R.id.btOpenLocation:
                int zoomInt = Integer.parseInt(etZoom.getText().toString());
                if (zoomInt < 1 || zoomInt > 23){
                    etZoom.setError("Valor introducido no válido");
                }else {
                    openLocation(etLocation.getText().toString(), etZoom.getText().toString());
                }
                break;
        }
    }

    private void openWebsite(String urlText){

        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openWebsite : Can't handle this intent!");
        }
    }

    private void openLocation(String location, String zoom){
        Uri addressUri = Uri.parse("geo:0, " + zoom + "?q=" + location);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openLocation : Can't handle this intent!");
        }
    }

    private void shareText(String text){
        new ShareCompat.IntentBuilder(this)
                .setType("text/plain")
                .setText(text)

                .startChooser();
    }
}