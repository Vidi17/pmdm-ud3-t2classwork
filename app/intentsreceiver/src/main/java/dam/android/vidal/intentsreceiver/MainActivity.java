package dam.android.vidal.intentsreceiver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){
        texto = findViewById(R.id.etTexto);

        ShareCompat.IntentReader intentReader = new ShareCompat.IntentReader(this);
        if (intentReader.isShareIntent()){
            String text = (String) intentReader.getText();
            texto.setText(text);
        }
    }
}